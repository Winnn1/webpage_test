import { useState } from 'react'
import './App.css'
import MainWeb from './webpage/lazadamain'
function App() {
  return (
    <>
      <div className="App">
        <MainWeb />
      </div>
    </>
  );
}

export default App;
