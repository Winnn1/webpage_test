import * as React from "react";
import Box from "@mui/material/Box";
import { Grid } from "@mui/material";
import prod1 from "./img/prod1.webp";
import Button from "@mui/material/Button";

function ProductList() {
  return (
    <Box sx={{ width: "100%", height: "100%" }}>
      <Grid container spacing={2} sx={{ justifyContent: "center" }}>
        <Grid item>
          <Button
            sx={{
              bgcolor: "#FFFFFF",
              width: "188px",
              height: "290px",
              display: "flex",
              flexDirection: "column",
            }}
          >
            <img
              src={prod1}
              alt="Product"
              style={{ width: "188px", height: "188px" }}
            />
            <span style={{ marginTop: "10px", textAlign: "center" }}>
              Product Name
            </span>
          </Button>
        </Grid>
        <Grid item>
          <Button
            sx={{
              bgcolor: "#FFFFFF",
              width: "188px",
              height: "290px",
              display: "flex",
              flexDirection: "column",
            }}
          >
            <img
              src={prod1}
              alt="Product"
              style={{ width: "188px", height: "188px" }}
            />
            <span style={{ marginTop: "10px", textAlign: "center" }}>
              Product Name
            </span>
          </Button>
        </Grid>
        <Grid item>
          <Button
            sx={{
              bgcolor: "#FFFFFF",
              width: "188px",
              height: "290px",
              display: "flex",
              flexDirection: "column",
            }}
          >
            <img
              src={prod1}
              alt="Product"
              style={{ width: "188px", height: "188px" }}
            />
            <span style={{ marginTop: "10px", textAlign: "center" }}>
              Product Name
            </span>
          </Button>
        </Grid>
        <Grid item>
          <Button
            sx={{
              bgcolor: "#FFFFFF",
              width: "188px",
              height: "290px",
              display: "flex",
              flexDirection: "column",
            }}
          >
            <img
              src={prod1}
              alt="Product"
              style={{ width: "188px", height: "188px" }}
            />
            <span style={{ marginTop: "10px", textAlign: "center" }}>
              Product Name
            </span>
          </Button>
        </Grid>
        <Grid item>
          <Button
            sx={{
              bgcolor: "#FFFFFF",
              width: "188px",
              height: "290px",
              display: "flex",
              flexDirection: "column",
            }}
          >
            <img
              src={prod1}
              alt="Product"
              style={{ width: "188px", height: "188px" }}
            />
            <span style={{ marginTop: "10px", textAlign: "center" }}>
              Product Name
            </span>
          </Button>
        </Grid>
        <Grid item>
          <Button
            sx={{
              bgcolor: "#FFFFFF",
              width: "188px",
              height: "290px",
              display: "flex",
              flexDirection: "column",
            }}
          >
            <img
              src={prod1}
              alt="Product"
              style={{ width: "188px", height: "188px" }}
            />
            <span style={{ marginTop: "10px", textAlign: "center" }}>
              Product Name
            </span>
          </Button>
        </Grid>
      </Grid>
    </Box>
  );
}
export default ProductList;
