import * as React from "react";
import Box from "@mui/material/Box";
import { List, ListItem, ListItemButton, ListItemText } from "@mui/material";

function DropDownList() {
  return (
    <div>
      <Box style={{ backgroundColor: "#FFFFFF" }}>
        <List>
          <ListItem disablePadding>
            <ListItemButton>
              <ListItemText primary="Electronic Device" />
            </ListItemButton>
          </ListItem>
          <ListItem disablePadding>
            <ListItemButton>
              <ListItemText primary="Electronic Accessories" />
            </ListItemButton>
          </ListItem>
          <ListItem disablePadding>
            <ListItemButton>
              <ListItemText primary="TV & Home Appliances" />
            </ListItemButton>
          </ListItem>
          <ListItem disablePadding>
            <ListItemButton>
              <ListItemText primary="Health & Beauty" />
            </ListItemButton>
          </ListItem>
          <ListItem disablePadding>
            <ListItemButton>
              <ListItemText primary="Barbies & Toys" />
            </ListItemButton>
          </ListItem>
          <ListItem disablePadding>
            <ListItemButton>
              <ListItemText primary="Groceries & Pets" />
            </ListItemButton>
          </ListItem>
          <ListItem disablePadding>
            <ListItemButton>
              <ListItemText primary="Home & Lifestyles" />
            </ListItemButton>
          </ListItem>
        </List>
      </Box>
    </div>
  );
}
export default DropDownList;
