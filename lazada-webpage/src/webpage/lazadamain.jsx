import * as React from "react";
import AppBar from "@mui/material/AppBar";
import Box from "@mui/material/Box";
import Toolbar from "@mui/material/Toolbar";
import IconButton from "@mui/material/IconButton";
import logo from "./img/logo.png";
import { styled } from "@mui/material/styles";
import InputBase from "@mui/material/InputBase";
import Button from "@mui/material/Button";
import SearchIcon from "@mui/icons-material/Search";
import { Grid, Typography } from "@mui/material";
import DropDownList from "./dropdown";
import ButtonList from "./button";
import mainimg from "./img/mainimg.webp";
import ProductList from "./product";

const Search = styled("div")(() => ({
  position: "relative",
  backgroundColor: "#e7e8ec",
  color: "#000000",
  width: "55%",
  height: "73%",
}));

function MainWeb() {
  return (
    <div>
      <Box sx={{ bgcolor: "#FFFFFF", height: "25px", position: "relative" }} />
      <Box sx={{ display: { xs: "none", sm: "block" } }}>
        <AppBar
          position="static"
          sx={{ height: 100, justifyContent: "center", bgcolor: "#FFFFFF" }}
        >
          <Toolbar sx={{ justifyContent: "center", minHeight: 100 }}>
            <IconButton sx={{ width: { xs: 150, sm: 200 } }}>
              <img src={logo} style={{ width: 127, height: 40 }} alt="Logo" />
            </IconButton>
            <Search>
              <InputBase
                placeholder="Search…"
                inputProps={{ "aria-label": "search" }}
                style={{ width: "60%", height: "100%", textAlign: "left" }}
              />
            </Search>
            <Button
              style={{
                backgroundColor: "#f57224",
                color: "#FFFFFF",
                width: "45px",
                height: "73%",
                borderRadius: 0,
              }}
            >
              <SearchIcon />
            </Button>
          </Toolbar>
        </AppBar>
      </Box>
      <Box
        sx={{
          width: "auto",
          backgroundImage: `url(${mainimg})`,
          backgroundSize: "cover",
          display: "flex",
          justifyContent: "center",
          alignItems: "center",
        }}
      >
        <Grid container>
          <Grid
            sx={{
              width: "180px",
              height: "400px",
              marginLeft: "330px",
              position: "relative",
            }}
          >
            <Box style={{ backgroundColor: "#FFFFFF" }}>
              <DropDownList />
            </Box>
          </Grid>
        </Grid>
      </Box>
      <ButtonList />
      <Typography
        sx={{ textAlign: "left", pl: "340px", pt: 2, fontSize: "20px" }}
      >
        Flash Sale
      </Typography>
      <Box
        sx={{
          width: "82%",
          height: "100%",
          display: "flex",
          justifyContent: "space-between",
        }}
      >
        <Typography sx={{ textAlign: "left", pl: "340px", pt: 2 }}>
          On Sale Now
        </Typography>
        <Button variant="outlined" style={{ color: "#f57224" }}>
          Shop All Product
        </Button>
      </Box>
      <ProductList />
      <Typography
        sx={{ textAlign: "left", pl: "340px", pt: 2, fontSize: "20px" }}
      >
        LazMall
      </Typography>
      <ProductList />
      <Box
        sx={{
          height: "120px",
          bgcolor: "#FFFFFF",
          display: "flex",
          flexDirection: "column",
          justifyContent: "center",
          textAlign: "right",
          pl: "210px",
        }}
      >
        <Typography sx={{ pr: 15 }}>© Lazada 2023</Typography>
      </Box>
    </div>
  );
}
export default MainWeb;
