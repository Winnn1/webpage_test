import * as React from "react";
import { Grid } from "@mui/material";
import Lazmall from "./img/Lazmall.png";
import Lazblog from "./img/Lazblog.png";
import Lazpick from "./img/Lazpick.png";
import Topup from "./img/Topup.png";
import Vouchers from "./img/Vouchers.png";
import Button from "@mui/material/Button";

function ButtonList() {
  return (
    <Grid container spacing={2} sx={{ pt: 2, pl: 3, justifyContent: "center" }}>
      <Grid item>
        <Button
          variant="text"
          sx={{
            width: "230px",
            height: "40px",
            bgcolor: "#f5f5f5",
            color: "#000000",
            borderRadius: 8,
          }}
        >
          <img src={Lazmall} style={{ width: 32, height: 32 }} />
          LazMall
        </Button>
      </Grid>
      <Grid item>
        <Button
          variant="text"
          sx={{
            width: "230px",
            height: "40px",
            bgcolor: "#f5f5f5",
            color: "#000000",
            borderRadius: 8,
          }}
        >
          <img src={Vouchers} style={{ width: 32, height: 32 }} />
          Vouchers
        </Button>
      </Grid>
      <Grid item>
        <Button
          variant="text"
          sx={{
            width: "230px",
            height: "40px",
            bgcolor: "#f5f5f5",
            color: "#000000",
            borderRadius: 8,
          }}
        >
          <img src={Topup} style={{ width: 32, height: 32 }} />
          Top-up,Bills&Food
        </Button>
      </Grid>
      <Grid item>
        <Button
          variant="text"
          sx={{
            width: "230px",
            height: "40px",
            bgcolor: "#f5f5f5",
            color: "#000000",
            borderRadius: 8,
          }}
        >
          <img src={Lazblog} style={{ width: 32, height: 32 }} />
          LazBlog
        </Button>
      </Grid>
      <Grid item>
        <Button
          variant="text"
          sx={{
            width: "230px",
            height: "40px",
            bgcolor: "#f5f5f5",
            color: "#000000",
            borderRadius: 8,
          }}
        >
          <img src={Lazpick} style={{ width: 32, height: 32 }} />
          LazPick
        </Button>
      </Grid>
    </Grid>
  );
}
export default ButtonList;
